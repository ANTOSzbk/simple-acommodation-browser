import { Text, Box, Spinner, Heading, Icon } from '@chakra-ui/core';
import React, { useState } from 'react';
import './App.scss';
import GuestForm from './components/forms/GuestForm';
import List from './components/list/List';
import IApiResponseData, { IItem } from './utils/DataInterface';

function App() {
  const [results, setResults] = useState<IApiResponseData | IItem[] | [] | Error>([]);
  const [loading, setLoading] = useState<Boolean | null>(null);
  return (
    <div className="page-wrapper">
      <Heading as="h1" textAlign="center" fontSize={{ md: "1.8rem", sm: "1.5rem", xs: "1.3rem" }}>Wybierz odpowiednie informacje, aby zobaczyć ceny</Heading>
      <GuestForm
        setResults={(data: IApiResponseData | Error) => setResults(data)}
        loading={(loading: Boolean) => setLoading(loading)} />
      {loading === null ? null :
        loading ?
          <Box textAlign="center" m={[8, 12, 16, 20]}>
            <Spinner
              width={["5rem", "6rem", "7rem", "8rem"]}
              height={["5rem", "6rem", "7rem", "8rem"]}
              speed={"0.7s"}
              m="auto"
              thickness="4px"
              emptyColor="gray.200"
              color="teal.500"
            />
          </Box> :
          results instanceof Error ?
            <Text fontSize={{ base: "1.5rem", md: "2rem", lg: "2.5rem", xl: "2.5rem" }} color="tomato" textAlign="center" m={4}>
              <Icon name="warning" /> {results.message}
            </Text> :
            <List data={results} />}
    </div>
  );
}

export default App;

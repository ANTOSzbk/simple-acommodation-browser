import React, { useState } from 'react';
import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Input,
  Collapse, NumberInput, NumberInputField, NumberInputStepper, NumberDecrementStepper, NumberIncrementStepper
} from "@chakra-ui/core";
import { Field } from "formik";
import { GuestFormInterface } from '../../utils/Schemas';

interface IProps {
  label: string,
  type: "button" | "checkbox" | "color" | "date" | "datetime-local" | "email" | "file" | "hidden" | "image" | "month" | "number" | "password" | "radio" | "range" | "reset" | "search" | "submit" | "tel" | "text" | "time" | "url" | "week",
  helperText: string,
  name: string,
  errors: GuestFormInterface | any,
  min?: number,
  max?: number,
}

const FieldInput: React.FC<IProps> = (props: IProps) => {
  const describedBy = `${props.name}-helper-text`;
  const [value, setValue] = useState();
  return (
    <Field name={props.name}>
      {({ field, form }: { field: any, form: any }) => (
        <FormControl width={{ xl: '215px', lg: '215px' }} ml={{ xl: 3, lg: 3 }} mr={{ xl: 3, lg: 3 }} isInvalid={form.errors[props.name] && form.touched}>
          <FormLabel htmlFor={props.name}>{props.label}</FormLabel>
          {props.type === "number" ?
            <NumberInput min={props.min} max={props.max} onChange={(value: any) => { form.setFieldValue(props.name, value); setValue(value.toString()); }}>
              <NumberInputField value={value} type="number" {...field} />
              <NumberInputStepper>
                <NumberIncrementStepper />
                <NumberDecrementStepper />
              </NumberInputStepper>
            </NumberInput>
            : <Input {...field} type={props.type} id={props.name} aria-describedby={describedBy} />}
          <FormHelperText id={describedBy}>
            {props.helperText}
          </FormHelperText>
          <Collapse isOpen={form.errors[props.name] && form.touched}>
            <FormErrorMessage>{props.errors[props.name]}</FormErrorMessage>
          </Collapse>
        </FormControl>
      )}
    </Field>)
}

export default FieldInput
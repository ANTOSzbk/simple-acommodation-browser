import { Button, Box } from '@chakra-ui/core';
import { Formik, Form } from 'formik';
import jsonp from 'jsonp';
import React from 'react';
import IApiResponseData from '../../utils/DataInterface';
import FieldInput from './FieldInput';
import { FaSearch } from "react-icons/fa"
import { GuestFormSchema } from '../../utils/Schemas';

interface IProps {
  setResults: (data: IApiResponseData | Error) => void,
  loading: (loading: boolean) => void,
}

const GuestForm: React.FC<IProps> = (props: IProps) => {
  const today = new Date(Date.now());
  const tomorrow = new Date(Date.now() + 86400 * 1000);

  const dateForInputValue = (date: Date) => date.toISOString().split('T')[0];

  return (
    <Box width={["100%", "100%", "90%", "90%"]} m="auto" height="100%" style={{ borderBottom: "5px solid rgba(0, 0, 0, .2)" }}>
      <Formik
        initialValues={{ dateFrom: dateForInputValue(today), dateTo: dateForInputValue(tomorrow), adults: 1, children: 0 }}
        onSubmit={(values, actions) => {
          props.loading(true);
          jsonp(
            `http://testapi.itur.pl/api.php?date_from=${values.dateFrom}&date_to=${values.dateTo}&nb_adults=${values.adults}&nb_children=${values.children}`,
            (err: Error | null, data: IApiResponseData) => {
              actions.setSubmitting(false);
              props.loading(false);
              if (err) {
                props.setResults(err);
                throw err;
              }
              props.setResults(data);
            }
          )
        }}
        validateOnChange
        validationSchema={GuestFormSchema}>
        {props => (
          <Form onSubmit={props.handleSubmit}>
            <Box p={4} display={{ md: "flex" }} justifyContent={["space-between", "space-evenly", "space-around", "center"]}>
              <FieldInput label="Pobyt od" name="dateFrom" type="date" helperText="" {...props} />
              <FieldInput label="Pobyt do" name="dateTo" type="date" helperText="" {...props} />
            </Box>
            <Box p={4} display={{ md: "flex" }} justifyContent={["space-between", "space-evenly", "space-around", "center"]}>
              <FieldInput label="Dorośli" name="adults" type="number" helperText="Wpisz liczbę dorosłych." min={0} {...props} />
              <FieldInput label="Dzieci" name="children" type="number" helperText="Wpisz liczbę dzieci." min={0} {...props} />
            </Box>
            <Box p={4} textAlign="center">
              <Button type="submit" width={["100%", "100%", "80%", "455px"]} isLoading={props.isSubmitting} variantColor="teal"><Box as={FaSearch} size="1em" d="inline-block" mr={2} /> Wyszukaj</Button>
            </Box>
          </Form>
        )}
      </Formik>
    </Box>)
}

export default GuestForm
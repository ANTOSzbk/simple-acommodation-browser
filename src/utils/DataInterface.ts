export interface IItem {
  id: number,
  name: string,
  roomType: string,
  maxNbGuests: number,
  totalPrice: number,
  bedroomsCount: number,
  singleBedsCount: number,
  doubleBedsCount: number,
  image: string,
}

export default interface IApiResponseData extends Array<IItem> { }